#include "Version.hpp"

namespace Stle {

const char *stleVersion() {
    return STLE_VERSION;
}

}
