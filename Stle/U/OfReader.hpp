#ifndef U_OF_READER_HPP
#define U_OF_READER_HPP

#include "Api.hpp"

namespace U {

struct U_API OfReaderTag {};
constexpr OfReaderTag OF_READER;

}

#endif
