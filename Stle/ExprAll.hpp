#ifndef STLE_EXPR_ALL_HPP
#define STLE_EXPR_ALL_HPP

#include "Expr.hpp"

#include "ConstExpr.hpp"
#include "BoolConstExpr.hpp"
#include "VarExpr.hpp"
#include "ApplyExpr.hpp"
#include "OnExpr.hpp"
#include "AggregateExpr.hpp"
#include "UntilExpr.hpp"
#include "GetExpr.hpp"
#include "LookupExpr.hpp"
#include "TemporalExpr.hpp"
#include "TemporalUntilExpr.hpp"

#endif
