STL Evaluator
=============

[![pipeline status](https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/tempo/StlEval/badges/master/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/tempo/StlEval/commits/master)

StlEval is developed at Verimag, Universite Grenoble Alpes by Alexey Bakhikin,
supported by the ERC project STATOR.

- [Building From Command Line](Build.md)
- [Coding Conventions](Coding.md)
